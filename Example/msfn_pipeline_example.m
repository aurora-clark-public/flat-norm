filename = 'WATER22500';
lambdas = [.4, .3, .2];
cutoffs = [1.05, .5, .34, .19, .15, .13, .11, .1, .09, .08];

%Embedding Willard-Chandler surface into 3D simplicial complex 
write_node_face_file(filename);
create_smesh_file(filename, 5);

%MSFN
write_bd_matrix(filename, 'upper');
msfn_main(filename, 'upper', lambdas);

%ComponentTracking
write_components(filename, lambdas, 'upper');
write_labels(filename, lambdas, 'upper');
compFileNames = {'components_upperWATER22500Lambda0.4000.txt', 'components_upperWATER22500Lambda0.3000.txt', 'components_upperWATER22500Lambda0.2000.txt'};
write_volumes(filename, compFileNames, lambdas, 'upper');
label_probabilities(filename, lambdas, cutoffs, 'upper', 1);
#!/usr/bin/python
# vim:fileencoding=utf-8
import sys
import cPickle as pickle
from math import ceil
import numpy as np
from WillardChandler import *
from skimage import measure
from MDAnalysis import Universe

PDB_FILE = sys.argv[1]
### Generating iso-surface from PDB coordinates file
universe = Universe(PDB_FILE)
xbox, ybox, zbox = universe.trajectory.ts.dimensions[:3]
water = universe.select_atoms("resname WATE")
water_atom = water.select_atoms("name OW or name HW1 or name HW2") #("name HW1 or name HW2 or name OW")

com = []
for res in xrange(water.residues.n_residues):
    res_com = water.residues[res].atoms.center_of_mass()
    com.append((res_com[0], res_com[1], res_com[2]))
com = np.array(com)

rmin = np.array([0., 0., 0.])
rmax = np.array([ceil(xbox), ceil(ybox), ceil(zbox)])
nn = (rmax - rmin).astype(np.int)

com /= rmax
com *= nn

g = Grid((0,0,0), nn, num=nn)
s = Stencil(g)
s.Gaussian(0.2)
g = g.cyclicShape(s)
c = np.zeros(g.shape, dtype=np.float32)
s.place(c, com)
g, c = g.foldBack(c, pbcDims = None, dtype=np.float32)
iso = c.max() - 0.95*c.ptp()

### Lewiner marching cubes algorithm to find surfaces in 3d volumetric data
verts, faces,  normals, values = measure.marching_cubes_lewiner(c, iso)

### Write interface information
np.savetxt(str(PDB_FILE)[:-4]+'_verts.txt', verts, delimiter='  ')
np.savetxt(str(PDB_FILE)[:-4]+'_faces.txt', faces, delimiter='  ')
###np.savetxt(str(PDB_FILE)[:-4]+'_normals.txt', normals, delimiter='  ')
###np.savetxt(str(PDB_FILE)[:-5]+'_values.txt', values, delimiter='  ')

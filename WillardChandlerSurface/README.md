# Willard-Chandler Instantaneous Interface Calculator
# ZHU LIU, zhu.liu@wsu.edu

Python2.7-based tool to calculate the intrinsic interface/surface
![](data/Willard_Chandler_Image_Frame22500.jpg)

The Willard-Chandler approach is proposed by Willard and Chandler[1]. For the specific liquid-liquid interface case, the density based cutoff routine is described in the author's previous paper.[2]
From the coordinates file (PDB) to the Willard-Chandler surface, this Python-based tool is using MDAnalysis[3], a Python library for post-processing molecular dynamics trajectory.  
This tool adopts the Lewiner marching cubes algorithm that implemented in the Python package scikit-image [4] to convert the Willard-Chandler surface into 3-dimentional volumetric data (e.g. faces and verts).

# How to use the code

To obtain the volumetric data (faces and verts) for the further calculation in Flat-Norm, the code can be run in the following command line using the example:

python GeneratingVolumetricData.py data/Water_Aqueous_Frame22500.pdb 

The outputs are faces and verts files that will be as the input for the Flat-Norm calculation.

# References

1. A. P. Willard and D. Chandler, “Instantaneous liquid interfaces”, The Journal of Physical Chemistry B 114, 1954–1958 (2010)
2. Z. Liu, T. Stecher, H. Oberhofer, K. Reuter and C. Scheurer, "Response properties at the dynamic water/dichloroethane liquid-liquid interfac", Molecular Physics, 116, 3409-3416 (2018)
3. N. Michaud-Agrawal, E. J. Denning, T. B. Woolf, and O. Beckstein. "MDAnalysis: A Toolkit for the Analysis of Molecular Dynamics Simulations", J. Comput. Chem. 32, 2319-2327 (2011) (https://www.mdanalysis.org)
4. T. Lewiner, H. Lopes, A. W. Vieira, and G. Tavares, “Efficient implementation of marching cubes' cases with topological guarantees”, Journal of Graphics Tools 8, 1–15 (2003) (https://scikit-image.org/docs/dev/api/skimage.measure.html#marching-cubes)

function [RV] = ratio_and_volume(label, verts, tet, compFileNames, lambdas)
    RV = zeros(2, length(lambdas));
    
    verts = verts(2:end, :);
    tet = tet(2:end, :);   
    
    p = transpose(verts);
    p = p(2:end, :);
    
    K = transpose(tet);
    K = K(2:end, :);
    
    for ll = 1:length(lambdas)
        lambda = lambdas(ll);
        compLabels = dlmread(compFileNames{ll});
        [~, m] = size(compLabels);
        if m == 1
            compLabels = transpose(compLabels);
        end
        
        [idx, ~] = find(compLabels(:, 1) == label);
        if ~isempty(idx)
            tetInLabel = compLabels(idx, :);
            tetInLabel = tetInLabel(1, 2:end);
            tetInLabel = tetInLabel(tetInLabel > 0);
            
            volume = sum(abs(simp_vol2(p, K(:, tetInLabel))));
            ratio = volume./((4./3).*pi.*(1./lambda).^3);
            
            RV(:, ll) = transpose([volume, ratio]);
        else
            continue;
        end
    end
end

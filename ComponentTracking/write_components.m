function [] = write_components(filename, lambdas, upperOrLower)
    filename = strcat(upperOrLower, filename);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Create dual graph of all tetrahedra for 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    neighFileName = strcat(filename, '.1.neigh');
    Neighbors = dlmread(neighFileName);
    neigh = Neighbors(2:end, :);

    %creating dual graph of tetrahedra       
    i = [];
    j = [];
      
    %creating i and j
    for aa = 1:size(neigh, 1)
        row_i = neigh(aa, 2:end);
        row_i = row_i(row_i ~= -1);
        columnNum = repmat(neigh(aa, 1), [1 length(row_i)]);
        i = cat(2, i, columnNum);
        j = cat(2, j, row_i);
    end

    %create cell array for node names the names correspond with the tet. ID
    numOfTet = size(neigh, 1);

    nodeNames = cell(1, numOfTet);
    for aa = 1: numOfTet
        nodeNames{aa} = int2str(aa);
    end

    adjacencyOfDual = sparse(i, j, 1);
    G = graph(adjacencyOfDual, nodeNames, 'omitselfloops');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %compute subgraph of S_lambda for largest lambda value.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    lambda = num2str(lambdas(1), '%.4f');

    S = dlmread(strcat(filename, 'Lambda', lambda, '.1.ele'));
    tetIDs = S(2:end, 1);

    subgraphNodeNames = cell(1, length(tetIDs));
    for aa = 1:length(tetIDs)
        subgraphNodeNames{aa} = int2str(tetIDs(aa));
    end

    H = subgraph(G, subgraphNodeNames);
    [bins, binsizes] = conncomp(H, 'OutputForm', 'cell');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %print tetrahedra for each connected component for S_lambda when lambda
    %is the largest lambda value.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    fcomponents = fopen(strcat('components_', filename, 'Lambda', lambda, '.txt'), 'wt');
    for ll = 1:length(binsizes)
        binNumbers = int_cell_2_vect(bins{ll});
        fprintf(fcomponents, '%i ', ll);
        fprintf(fcomponents, '%g\t', binNumbers);
        fprintf(fcomponents, '\n');
    end
    fclose(fcomponents);
    
    componentLabels = 1:length(binsizes);
    maxOfComponentLabels = max(componentLabels);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %compute dual graph for S_{lambda_{i + 1}}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for jj = 2:length(lambdas)
        nextLambda = num2str(lambdas(jj), '%.4f');
        
        nextS = dlmread(strcat(filename, 'Lambda', nextLambda, '.1.ele'));
        
        nextTetIDs = nextS(2:end, 1);

        nextSubgraphNodeNames = cell(1, length(nextTetIDs));
        for aa = 1:length(nextTetIDs)
            nextSubgraphNodeNames{aa} = int2str(nextTetIDs(aa));
        end

        nextH = subgraph(G, nextSubgraphNodeNames);
        [nextBins, nextBinsizes] = conncomp(nextH, 'OutputForm', 'cell');

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %print new components for S_{lambda_{i + 1}}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        totalNextBinLabels = [];
        newFcomponents = fopen(strcat('components_', filename, 'Lambda', nextLambda, '.txt'), 'wt');
        for ll = 1: length(nextBinsizes)
            nextBinNumbers = int_cell_2_vect(nextBins{ll});
            
            prevComp = dlmread(strcat('components_', filename, 'Lambda', num2str(lambdas(jj-1), '%.4f'), '.txt'));
    
            nextBinIntersections = [];
            for kk = 1: length(prevComp(:, 1))
                allBinNumbers = prevComp(kk, 1:end);
                binNumbers = allBinNumbers(2:end);
                binNumbers = binNumbers(binNumbers > 0);
        
                if ~isempty(intersect(binNumbers, nextBinNumbers))
                    nextBinIntersections = cat(2, nextBinIntersections, allBinNumbers(1));
                end 
            end
    
            if length(nextBinIntersections) == 1
                nextBinLabel = nextBinIntersections(1);
                
            else
                nextBinLabel = maxOfComponentLabels + 1;
                maxOfComponentLabels = maxOfComponentLabels + 1;
            end
            
            fprintf(newFcomponents, '%i ', nextBinLabel);
            fprintf(newFcomponents, '%g\t', nextBinNumbers);
            fprintf(newFcomponents, '\n');
            
            totalNextBinLabels = cat(2, totalNextBinLabels, nextBinLabel);
        end
        fclose(newFcomponents);
    end
end

function [] = write_volumes(filename, compFileNames, lambdas, upperOrLower)
    filename = strcat(upperOrLower, filename);
    verts = dlmread(strcat(filename, '.1.node'));
    tets = dlmread(strcat(filename, '.1.ele'));

    volFile = fopen(strcat(filename, '-volumes.txt'), 'wt'); 
    RV = ratio_and_volume(1, verts, tets, compFileNames, lambdas);
    volumes = RV(1, :);
        
    fprintf(volFile, '%i ', 1);
    for ii = 1:length(lambdas)
        fprintf(volFile, '%g ', volumes(ii));
    end
    fprintf(volFile, '\n');
    
    cc = 2;
    while ~isempty(volumes(volumes > 0))
        RV = ratio_and_volume(cc, verts, tets, compFileNames, lambdas);
        volumes = RV(1, :);
        
        fprintf(volFile, '%i ', cc);
        for ii = 1:length(lambdas)
            fprintf(volFile, '%g ', volumes(ii));
        end
        fprintf(volFile, '\n');
        cc = cc + 1;
    end 
    fclose(volFile);
end

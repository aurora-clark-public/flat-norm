function [] = write_labels(filename, lambdas, upperOrLower)
    filename = strcat(upperOrLower, filename);
    
    components_last = dlmread(strcat('components_', filename, 'Lambda', num2str(lambdas(1), '%.4f'), '.txt'));
    maxID = max(components_last(:, 1));
    
    allCompsMerged = 0;
    
    for ll = 2:length(lambdas)
        lambda = lambdas(ll); 
        components_current = dlmread(strcat('components_', filename, 'Lambda', num2str(lambda, '%.4f'), '.txt'));
        [n, m] = size(components_current);
        
        if m == 1
            if allCompsMerged == 0
                allCompsMerged = 1;
                components_current(1) = min(components_last(:, 1));
            else
                components_current(1) = components_last(1);
            end
            
            file_current = fopen(strcat('components_', filename, 'Lambda', num2str(lambda, '%.4f'), '.txt'), 'wt');
            fprintf(file_current, '%i ', components_current);
            fprintf(file_current, '\n');
            fclose(file_current);
        
        else
        
            for ii_current = 1:n
                comp = components_current(ii_current, 2:end);
                nonZeroTetInComp = comp(comp > 0);
            
                %the ID for the component is new which means it the component is new or two previous components merged.
                compIDsWithNonEmptyInt = [];
            
                for ii_last = 1:size(components_last, 1)
                    compID_last = components_last(ii_last, 1);
                    comp_last = components_last(ii_last, 2:end);
                    nonZeroTetInComp_last = comp_last(comp_last > 0);
                
                    if ~isempty(intersect(nonZeroTetInComp, nonZeroTetInComp_last))
                        compIDsWithNonEmptyInt = cat(2, compIDsWithNonEmptyInt, compID_last);
                    end
                end
                
                if ~isempty(compIDsWithNonEmptyInt)%the new component came from a merging behavior or was there before 
                    components_current(ii_current, 1) = min(compIDsWithNonEmptyInt);
                
                else%the new component is actually new and the tetrahedra were not there before
                    components_current(ii_current, 1) = maxID + 1;
                    maxID = maxID + 1;
                end
            end
            
            file_current = fopen(strcat('components_', filename, 'Lambda', num2str(lambda, '%.4f'), '.txt'), 'wt');
            for ii_current = 1:size(components_current, 1)
                componentID = components_current(ii_current, 1);
                componentTet = components_current(ii_current, 2:end);
                componentTet = componentTet(componentTet > 0);
                
                fprintf(file_current, '%i ', componentID);
                fprintf(file_current, '%g\t', componentTet);
                fprintf(file_current, '\n');
            end
            fclose(file_current);
        end
        
        components_last = components_current;
    end
end
function [probabilities] = label_probabilities(filename, lambdas, cutoffs, upperOrLower, write)
    filename = strcat(upperOrLower, filename);
    volumes = dlmread(strcat(filename, '-volumes.txt'));
    
    probabilities = zeros(size(volumes, 1), 1);
    for ii = 1:size(volumes, 1)
        numberOfAlivePairs = 0;
        for jj = 2:size(volumes, 2)
            volume = volumes(ii, jj);
            lambda = lambdas(jj - 1);
            ratio = volume./((4./3).*pi.*(1./lambda).^3);
            bool_cut = repmat(ratio, 1, length(cutoffs)) > cutoffs;
            bool_vol = repmat(volume, 1, length(cutoffs)) > 50;
            numberOfAlivePairs = numberOfAlivePairs + sum(bool_cut.*bool_vol);
        end
        probabilities(ii) = numberOfAlivePairs;
    end
    
    probabilities = probabilities./(length(lambdas).*length(cutoffs));
    
    %if write = 1 then the user wants to write the probability into a file.
    if write == 1
        probFile = fopen(strcat(filename, '-probabilities.txt'), 'wt');
        for pp = 1:size(probabilities, 1)
            fprintf(probFile, '%i %g\n', [pp, probabilities(pp)]);
        end
        fclose(probFile);
    end
end

function [] = write_bd_matrix(filename, upperOrLower)

    filename = strcat(upperOrLower, filename);
    nodeFileName = strcat(filename, '.1.node');
    faceFileName = strcat(filename, '.1.face');
    tetFileName = strcat(filename, '.1.ele');
    t2fFileName = strcat(filename, '.1.t2f');
    
    existsNodes = dir(nodeFileName);
    existsFaces = dir(faceFileName);
    existsTets = dir(tetFileName);
    existsTetToFace = dir(t2fFileName);
    
    n1 = size(existsNodes, 1);
    n2 = size(existsFaces, 1);
    n3 = size(existsTets, 1);
    n4 = size(existsTetToFace, 1);
    
    if n1 > 0 && n2 > 0 && n3 > 0 && n4 > 0
        NodeCoor = dlmread(nodeFileName);
        Faces = dlmread(faceFileName);
        Tetrahedra = dlmread(tetFileName);
        t2f = dlmread(t2fFileName);

        %Node, Face, and Tetrahedra matrices
        N = NodeCoor(2: end, :);
        F = Faces(2: end, 1: 4);
        T = Tetrahedra(2: end, :);

        %Orient your Tetrahedra consistently
        orientedT = orient3DComplex(N, T);

        %Put node, face, and oriented tet. matrices in correct format and syntax
        %(syntax is unnecessary, it is only done for documentation purposes).
        p = transpose(N);
        p = p(2:end, :);

        K = transpose(orientedT);
        K = K(2:end, :);

        M = transpose(F);
        M = M(2:end, :);

        % n = number of simplices, s(1:n)
        n = size(K, 2);
        % m = number of codimension 1 simplices
        m = size(M, 2);

        d = size(K, 1) - 2;
    
        [K, kcolindex] = sort(K, 1);
        kparity = permutation_parity(kcolindex,1);
        [M, mcolindex] = sort(M, 1);
        mparity = permutation_parity(mcolindex,1);
        
        fileID = fopen(strcat('const_', filename, '.txt'), 'w');

        for kk = 1:n
            kk
            for jj = 1:d+2
                ssidx = t2f(kk, jj + 1);
                tet = K(:, kk);
                face = M(:, ssidx);
            
                if tet(1) == face(1)
                    if tet(2) == face(2)
                        if tet(3) == face(3)
                            corIndex = 4;
                        else
                            corIndex = 3;
                        end
                    else
                        corIndex = 2;
                    end
                else
                    corIndex = 1;
                end
                val = (-1)^(corIndex + 1 + kparity(kk) + mparity(ssidx));
            
                fprintf(fileID,'%d %d %d\n',ssidx,kk, val);
            end
        end
        fclose(fileID);
    else
        'does not exist filename'
    end
end



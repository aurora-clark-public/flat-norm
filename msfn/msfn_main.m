function [] = msfn_main(filename, upperOrLower, lambdas)
    filename = strcat(upperOrLower, filename);
    
    nodeFileName = strcat(filename, '.1.node');
    faceFileName = strcat(filename, '.1.face');
    tetFileName = strcat(filename, '.1.ele');
    constFileName = strcat('const_', filename, '.txt');


    NodeCoor = dlmread(nodeFileName);
    Faces = dlmread(faceFileName);
    Tetrahedra = dlmread(tetFileName);

    %Node, Face, and Tetrahedra matrices
    N = NodeCoor(2: end, :);
    F = Faces(2: end, 1: 4);
    T = Tetrahedra(2: end, :);

    %Orient your Tetrahedra consistently
    orientedT = orient3DComplex(N, T);

    %Put node, face, and oriented tet. matrices in correct format and syntax
    %(syntax is unnecessary, it is only done for documentation purposes).
    p = transpose(N);
    p = p(2:end, :);

    K = transpose(orientedT);
    K = K(2:end, :);

    M = transpose(F);
    M = M(2:end, :);

    preConst = dlmread(constFileName);

    bd = sparse(preConst(:, 1), preConst(:, 2), preConst(:, 3));
    m = size(bd, 1);
    n = size(bd, 2);
    const = cat(2, speye(m), -speye(m), bd, -bd);

    %Obtaining the orientations for t_W3 are made quicker with the already
    %constructed (by TetGen) identification of which triangles in the face file
    %are faces of which tetrahedra.
    FacesWithBdLabel = Faces(2:end , 1: end - 2);
    faceAndTet = Faces(2:end, 1:end);
    numOfFaces = size(FacesWithBdLabel, 1);

    %Constructing input vector's non-zero entries
    t = [];

    for i = 1: numOfFaces
        if FacesWithBdLabel(i, 5) == 0
            t = [t 0];
        end

        if FacesWithBdLabel(i, 5) == 1
            t = [t 1];
        end
    end

    %Orient the input vector so it is consistent.
    t = orientInputCurrent2(t, faceAndTet, bd);
    t = transpose(t);

    v = simp_vol2(p, K);
    w = face_areas(p, M);

    for kk = 1: length(lambdas)
        lamb = lambdas(kk);
        [norm, x, s, ~, ~, const] = msfn(p, K, M, t, lamb, v, w, const);

        strcat('done computing msfn lambda ', num2str(lamb))

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %writing files for S_lambda, T-Bd(S_lambda) and its nodes
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        strLamb = num2str(lamb, '%.4f');
        
        %writing flat surface nodes
        flatSurface = faceAndTet(find(x), :);
        numOfFacesInFlatSurface = size(flatSurface, 1);
        nodesInFlatSurface = unique(reshape(flatSurface(:, 2:4), [1, 3*numOfFacesInFlatSurface]));
        coordForNodesInFlatSurface = N(nodesInFlatSurface, 2:end);

        fnode = fopen(strcat(filename, 'Lambda', strLamb, '.1.node'), 'wt');
        for ll = 1:size(coordForNodesInFlatSurface, 1)
            fprintf(fnode, '%.9f %.9f %.9f\n', coordForNodesInFlatSurface(ll, :));
        end
        fclose(fnode);

        %writing flat surface, x = T-Bd(S_\lambda) into .face file
        flatNodesID = N(nodesInFlatSurface, 1);
        
        MAX = max([max(flatSurface(2, :)), max(flatSurface(3, :)), max(flatSurface(4, :))]);
        map = zeros(MAX, 1);
    
        for jj = 1: size(coordForNodesInFlatSurface, 1)
            map(flatNodesID(jj)) = jj;
        end
    
        for mm = 1: size(flatSurface, 1)
            for ll = 2:4
                flatSurface(mm, ll) = map(flatSurface(mm, ll));
            end
        end

        fid = fopen(strcat(filename, 'Lambda', strLamb, '.1.face'),'wt');
        fprintf(fid, '%g\t', [size(flatSurface, 1) 1]);
        fprintf(fid, '\n');
        for ll = 1:size(flatSurface, 1)
            fprintf(fid, '%g\t', flatSurface(ll, :));
            fprintf(fid, '\n');
        end
        fclose(fid);

        %writing s = S_\lambda tetrahedra into .ele file
        [idx, ~] = find(s);
        S = Tetrahedra(idx + 1, :);

        fvol = fopen(strcat(filename, 'Lambda', strLamb, '.1.ele'),'wt');
        fprintf(fvol, '%g\t', [size(S, 1) 4 0]);
        fprintf(fvol, '\n');
        for ll = 1:size(S, 1)
            tet = S(ll, :);
            fprintf(fvol, '%g\t', tet);
            fprintf(fvol, '\n');
        end
        fclose(fvol);

        %writing flat norm value
        fNorm = fopen(strcat(filename, 'Lambda', strLamb, 'NORM.txt'), 'wt');
        fprintf(fNorm, '%.9f', norm);
        fclose(fNorm);
    end
end

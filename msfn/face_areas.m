function v = face_areas(p,t)
%SIMPVOL2 Simplex volume in Euclidean space.
%   V=SIMPVOL2(P,T)
% p = matrix of point coordinates.  Each column gives the
%   coordinates of a single point in Euclidean space.
% t = matrix of simplices.  Each column corresponds to a
%   single d-dimensional simplex and has d+1 indices corresponding
%   to columns of p.

% V = vector of simplex volumes, one entry per column of t

% Notes:
% simpvol from DistMesh can not calculate volumes of d-dimensional
% simplices where d is less than the dimension of the ambient space.
% This function does so by projecting into the d-dimensional
% subspace containing the simplex and doing the calculation there.
% This will fail if the d+1 points are contained in a subspace of
% dimension d-1 or less.
% Note that p and t are transposed from their use in simpvol.
% This is so that this function can be more easily called from 
% msfn and so that things are processed in a column major order.

% d = simplex dimension
d = size(t, 1) - 1;

% Initialize simplex volume array
v = zeros(size(t, 2), 1);

minX = min(p(1, :));
maxX = max(p(1, :));
minY = min(p(2, :));
maxY = max(p(2, :));

for ii = 1: size(t, 2)
    pts = p(:, t(:, ii));
    
    sx_1 = sum(pts(1, :) == [minX, minX, minX]);
    sx_2 = sum(pts(1, :) == [maxX, maxX, maxX]);
    sy_1 = sum(pts(2, :) == [minY, minY, minY]);
    sy_2 = sum(pts(2, :) == [maxY, maxY, maxY]);
    
    sums = [sx_1, sx_2, sy_1, sy_2];
    
    YesOrNo = ismember(3, sums);
    
    if YesOrNo == 1
        v(ii) = 0;
    
    else
        vec = pts(:, 2: end) - pts(:, 1)*ones(1, d);
        ob = orth(vec);
        v(ii) = det(vec'*ob)/factorial(d);
    end
end

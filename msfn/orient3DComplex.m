function [Oriented3DComplex] = orient3DComplex(P, T)
    m = size(T, 1);
    Oriented3DComplex = zeros(m, 5);

    for i = 1: m
        VecMatrix = repmat(P(T(i, 2), 2: end), 3, 1);
        Matrix = P(T(i, 3:5), 2:4) - VecMatrix;
    
        if sign(det(Matrix)) == -1
            Oriented3DComplex(i, :) = [T(i, 1), T(i, 3), T(i, 2), T(i, 4:5)];
        end
    
        if sign(det(Matrix)) == 1
            Oriented3DComplex(i, :) = T(i, :);
        end
    end
end
    
function [orientedInputCurrent2] = orientInputCurrent2(inputCurrentVec, faceToTet, bd)
    orientedInputCurrent2 = inputCurrentVec;   
    
    nonzeroInput = find(inputCurrentVec);
    nonzeroFaces = faceToTet(nonzeroInput, :);
    s = size(nonzeroFaces, 1);
    
    for jj = 1: s
        orientedInputCurrent2(nonzeroInput(jj)) = bd(nonzeroFaces(jj, 1), nonzeroFaces(jj, 6));
    end
end
function [] = write_node_face_file(filename)
    Nodes = dlmread(strcat(filename, '_verts.txt'));
    Faces = dlmread(strcat(filename, '_faces.txt'));

    Faces = Faces + 1;

    numOfFaces = size(Faces, 1);
    numOfNodes = size(Nodes, 1);

    nodeFileName = strcat(filename, '.node');
    nodeFile = fopen(nodeFileName, 'w');

    fprintf(nodeFile, '%i 3 0 0\n', numOfNodes);
    for ii = 1: numOfNodes
        fprintf(nodeFile, '%i %.19g %.19g %.19g\n', [ii Nodes(ii, :)]);
    end

    faceFileName = strcat(filename, '.face');
    faceFile = fopen(faceFileName, 'w');

    fprintf(faceFile, '%i 1 0 0 0\n', numOfFaces);
    for ii = 1: numOfFaces
        fprintf(faceFile, '%i %i %i %i 1\n', [ii Faces(ii, :)]);
    end
end

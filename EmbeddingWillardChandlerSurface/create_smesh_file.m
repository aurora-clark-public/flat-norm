function [] = create_smesh_file(filename, nn)
    smeshUpperName = strcat('upper', filename, '.smesh');
    smeshLowerName = strcat('lower', filename, '.smesh');

    Nodes = dlmread(strcat(filename, '.node'));
    Faces = dlmread(strcat(filename, '.face'));
    Edges = meshEdges(Faces(2:end, 2:4));

    [oLowerMinX, oUpperMinX, oLowerMaxX, oUpperMaxX, oLowerMinY, oUpperMinY, oLowerMaxY, oUpperMaxY] = order_side_nodes(Nodes, Edges);

    coordLowerMinX = Nodes(oLowerMinX + 1, :);
    coordUpperMinX = Nodes(oUpperMinX + 1, :);
    coordLowerMaxX = Nodes(oLowerMaxX + 1, :);
    coordUpperMaxX = Nodes(oUpperMaxX + 1, :);
    coordLowerMinY = Nodes(oLowerMinY + 1, :);
    coordUpperMinY = Nodes(oUpperMinY + 1, :);
    coordLowerMaxY = Nodes(oLowerMaxY + 1, :);
    coordUpperMaxY = Nodes(oUpperMaxY + 1, :);

    MAXOFLOWER = max([coordLowerMinX(:, 4).', coordLowerMaxX(:, 4).', coordLowerMinY(:, 4).', coordLowerMaxY(:, 4).']);
    MINOFUPPER = min([coordUpperMinX(:, 4).', coordUpperMaxX(:, 4).', coordUpperMinY(:, 4).', coordUpperMaxY(:, 4).']);

    DIFFERENCE = abs(MINOFUPPER - MAXOFLOWER);

    DELTA = DIFFERENCE./nn;

    [NUMBEROFUPPER, ~] = size(Nodes(Nodes(2:end, 4) >= MINOFUPPER - DELTA, :));
    [NUMBEROFLOWER, ~] = size(Nodes(Nodes(2:end, 4) <= MAXOFLOWER + DELTA, :));
    TOTAL = Nodes(1, 1);

    %WARNING OUTPUT
    if NUMBEROFUPPER + NUMBEROFLOWER == length(unique(Faces(2:end, 2:4)))
        'lower + upper = total'
    end
    if NUMBEROFUPPER + NUMBEROFLOWER < length(unique(Faces(2:end, 2:4)))
        'lower + upper < total'
    end
    if NUMBEROFUPPER + NUMBEROFLOWER > length(unique(Faces(2:end, 2:4)))
        'lower + upper > total'
    end
    
    [Z1u, Z2u, Z3u, Z4u] = deal(MINOFUPPER - DELTA);
    [Z1l, Z2l, Z3l, Z4l] = deal(MAXOFLOWER + DELTA);

    [X1u, X2u, X1l, X2l] = deal(min(Nodes(2:end, 2)));
    [X3u, X4u, X3l, X4l] = deal(max(Nodes(2:end, 2)));

    [Y1u, Y3u, Y1l, Y3l] = deal(min(Nodes(2:end, 3)));
    [Y2u, Y4u, Y2l, Y4l] = deal(max(Nodes(2:end, 3)));

    idx = zeros(2, 4);
    idx(1, :) = [TOTAL + 1, TOTAL + 2, TOTAL + 3, TOTAL + 4];
    idx(2, :) = [TOTAL + 5, TOTAL + 6, TOTAL + 7, TOTAL + 8];

    uMinXMinYNode = [idx(1, 1), X1u, Y1u, Z1u];
    uMinXMaxYNode = [idx(1, 2), X2u, Y2u, Z2u];
    uMaxXMinYNode = [idx(1, 3), X3u, Y3u, Z3u];
    uMaxXMaxYNode = [idx(1, 4), X4u, Y4u, Z4u];

    lMinXMinYNode = [idx(2, 1), X1l, Y1l, Z1l];
    lMinXMaxYNode = [idx(2, 2), X2l, Y2l, Z2l];
    lMaxXMinYNode = [idx(2, 3), X3l, Y3l, Z3l];
    lMaxXMaxYNode = [idx(2, 4), X4l, Y4l, Z4l];

    Nodes = cat(1, Nodes, uMinXMinYNode, uMinXMaxYNode, uMaxXMinYNode, uMaxXMaxYNode, lMinXMinYNode, lMinXMaxYNode, lMaxXMinYNode, lMaxXMaxYNode);
    Nodes(1, 1) = size(Nodes, 1) - 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating Z-Plane faces order
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    uZFace = [idx(1, 1), idx(1, 2), idx(1, 4), idx(1, 3)];
    lZFace = [idx(2, 1), idx(2, 2), idx(2, 4), idx(2, 3)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating MinX-Plane faces order
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%upper min X-plane face order
    if Nodes(oUpperMinX(1) + 1, 3) < Nodes(oUpperMinX(end) + 1, 3)
        uMinXFace = cat(1, idx(1, 2), idx(1, 1), oUpperMinX);
    else
        uMinXFace = cat(1, idx(1, 1), idx(1, 2), oUpperMinX);
    end

%lower min X-plane face order
    if Nodes(oLowerMinX(1) + 1, 3) < Nodes(oLowerMinX(end) + 1, 3)
        lMinXFace = cat(1, idx(2, 2), idx(2, 1), oLowerMinX);
    else
        lMinXFace = cat(1, idx(2, 1), idx(2, 2), oLowerMinX);
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating MaxX-Plane faces order
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%upper max X-plane face order
    if Nodes(oUpperMaxX(1) + 1, 3) < Nodes(oUpperMaxX(end) + 1, 3)
        uMaxXFace = cat(1, idx(1, 4), idx(1, 3), oUpperMaxX);
    else
        uMaxXFace = cat(1, idx(1, 3), idx(1, 4), oUpperMaxX);
    end

%lower max X-plane face order
    if Nodes(oLowerMaxX(1) + 1, 3) < Nodes(oLowerMaxX(end) + 1, 3)
        lMaxXFace = cat(1, idx(2, 4), idx(2, 3), oLowerMaxX);
    else
        lMaxXFace = cat(1, idx(2, 3), idx(2, 4), oLowerMaxX);
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating Min Y Plane faces order
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%upper minY-plane face order
    if Nodes(oUpperMinY(1) + 1, 2) < Nodes(oUpperMinY(end) + 1, 2)
        uMinYFace = cat(1, idx(1, 3), idx(1, 1), oUpperMinY);
    else
        uMinYFace = cat(1, idx(1, 1), idx(1, 3), oUpperMinY);
    end

%lower minY-plane face order
    if Nodes(oLowerMinY(1) + 1, 2) < Nodes(oLowerMinY(end) + 1, 2)
        lMinYFace = cat(1, idx(2, 3), idx(2, 1), oLowerMinY);
    else
        lMinYFace = cat(1, idx(2, 1), idx(2, 3), oLowerMinY);
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating Max Y Plane face order
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%upper maxY-plane face order
    if Nodes(oUpperMaxY(1) + 1, 2) < Nodes(oUpperMaxY(end) + 1, 2)
        uMaxYFace = cat(1, idx(1, 4), idx(1, 2), oUpperMaxY);
    else
        uMaxYFace = cat(1, idx(1, 2), idx(1, 4), oUpperMaxY);
    end

%lower maxY-plane face order
    if Nodes(oLowerMaxY(1) + 1, 2) < Nodes(oLowerMaxY(end) + 1, 2)
        lMaxYFace = cat(1, idx(2, 4), idx(2, 2), oLowerMaxY);
    else
        lMaxYFace = cat(1, idx(2, 2), idx(2, 4), oLowerMaxY);
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%writing .smesh file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%write the .smesh file by first writing the nodes and then writing the
%faces
    upperfileName = smeshUpperName;
    lowerfileName = smeshLowerName;

    upperFile = fopen(upperfileName, 'w');
    lowerFile = fopen(lowerfileName, 'w');
%writing the nodes into .smesh file. Note that there are no new nodes being
%added, unlike the files created when we only looked at one surface at a
%time.
    for ii = 1: size(Nodes, 1)
        fprintf(upperFile, '%.19g %.19g %.19g %.19g\n', Nodes(ii, :));
        fprintf(lowerFile, '%.19g %.19g %.19g %.19g\n', Nodes(ii, :));
    end

%writing the original faces from Faces matrix into .smesh file
    fprintf(upperFile, '%i ', Faces(1, 1) + 5);
    fprintf(lowerFile, '%i ', Faces(1, 1) + 5);
    fprintf(upperFile, '%i\n', Faces(1, 2));
    fprintf(lowerFile, '%i\n', Faces(1, 2));

    for ii = 2: size(Faces, 1)
        fprintf(upperFile, '3 %i %i %i %i\n', Faces(ii, 2:end));
        fprintf(lowerFile, '3 %i %i %i %i\n', Faces(ii, 2:end));
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%writing the MinX-Faces into the .smesh file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%writing the lower MinX-Face into the lower_.smesh file
    LOWERMINX = length(lMinXFace);

    fprintf(lowerFile, '%i ', LOWERMINX);
    for ii = 1: LOWERMINX
        fprintf(lowerFile, '%i ', lMinXFace(ii));
    end
    fprintf(lowerFile, '0');

%writing the upper MinX-Face into the .smesh file
    UPPERMINX = length(uMinXFace);

    fprintf(upperFile, '\n%i ', UPPERMINX);
    for ii = 1: UPPERMINX
        fprintf(upperFile, '%i ', uMinXFace(ii));
    end
    fprintf(upperFile, '0');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%writing the MaxX-Faces into the .smesh file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%writing the lower MaxX-Face into the lower_.smesh file
    LOWERMAXX = length(lMaxXFace);

    fprintf(lowerFile, '\n%i ', LOWERMAXX);
    for ii = 1: LOWERMAXX
        fprintf(lowerFile, '%i ', lMaxXFace(ii));
    end
    fprintf(lowerFile, '0');

%writing the upper MaxX-Face into the .smesh file
    UPPERMAXX = length(uMaxXFace);

    fprintf(upperFile, '\n%i ', UPPERMAXX);
    for ii = 1: UPPERMAXX
        fprintf(upperFile, '%i ', uMaxXFace(ii));
    end
    fprintf(upperFile, '0');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%writing the MinY-Faces into the .smesh file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%writing the lower MinY-Face into the .smesh file
    LOWERMINY = length(lMinYFace);

    fprintf(lowerFile, '\n%i ', LOWERMINY);
    for ii = 1: LOWERMINY
        fprintf(lowerFile, '%i ', lMinYFace(ii));
    end
    fprintf(lowerFile, '0');

%writing the upper MinY-Face into the .smesh file
    UPPERMINY = length(uMinYFace);

    fprintf(upperFile, '\n%i ', UPPERMINY);
    for ii = 1: UPPERMINY
        fprintf(upperFile, '%i ', uMinYFace(ii));
    end
    fprintf(upperFile, '0');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%writing the MaxY-Faces into the .smesh file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%writing the lower MaxY-Face into the .smesh file
    LOWERMAXY = length(lMaxYFace);

    fprintf(lowerFile, '\n%i ', LOWERMAXY);
    for ii = 1: LOWERMAXY
        fprintf(lowerFile, '%i ', lMaxYFace(ii));
    end
    fprintf(lowerFile, '0');

%writing the upper MaxY-Face into the .smesh file
    UPPERMAXY = length(uMaxYFace);

    fprintf(upperFile, '\n%i ', UPPERMAXY);
    for ii = 1: UPPERMAXY
        fprintf(upperFile, '%i ', uMaxYFace(ii));
    end
    fprintf(upperFile, '0');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%writing the two uZFace, lZFace faces into the .smesh file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%writing the lower MaxY-Face into the .smesh file
    LOWERZ = length(lZFace);

    fprintf(lowerFile, '\n%i ', LOWERZ);
    for ii = 1: LOWERZ
        fprintf(lowerFile, '%i ', lZFace(ii));
    end
    fprintf(lowerFile, '0');

%writing the upper MaxX-Face into the .smesh file
    UPPERZ = length(uZFace);

    fprintf(upperFile, '\n%i ', UPPERZ);
    for ii = 1: UPPERZ
        fprintf(upperFile, '%i ', uZFace(ii));
    end
    fprintf(upperFile, '0');
        
%closing files to prevent matlab instability
    fclose(upperFile);
    fclose(lowerFile);
end

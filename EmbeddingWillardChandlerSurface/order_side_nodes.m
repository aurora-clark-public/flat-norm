%Author:        Enrique Alvarado
%Last Update:   3/6/19
%Input:         nodes and edges as matrices
%Ouput:         ordered__:  ordered nodes on the 4 faces to then use as
%                           facets in tetgen.

function [oLowerMinX, oUpperMinX, oLowerMaxX, oUpperMaxX, oLowerMinY, oUpperMinY, oLowerMaxY, oUpperMaxY] = order_side_nodes(Nodes, Edges)

    nodesWithIdx = Nodes(2:end, :);
    
    minXCoord = min(nodesWithIdx(:, 2));
    maxXCoord = max(nodesWithIdx(:, 2));
    minYCoord = min(nodesWithIdx(:, 3));
    maxYCoord = max(nodesWithIdx(:, 3));
       
    edgesInMinX = [];
    edgesInMaxX = [];
    edgesInMinY = [];
    edgesInMaxY = [];

   for ii = 1: size(Edges, 1)
       
        edge = Edges(ii, :);
        edgesNodesCoord = nodesWithIdx(edge, :);
        
        if sum(edgesNodesCoord(:, 2) == minXCoord) == 2
            edgesInMinX = cat(1, edgesInMinX, transpose(edgesNodesCoord(:, 1)));
        end
        
        if sum(edgesNodesCoord(:, 2) == maxXCoord) == 2
            edgesInMaxX = cat(1, edgesInMaxX, transpose(edgesNodesCoord(:, 1)));
        end

        if sum(edgesNodesCoord(:, 3) == minYCoord) == 2
            edgesInMinY = cat(1, edgesInMinY, transpose(edgesNodesCoord(:, 1)));
        end
        
        if sum(edgesNodesCoord(:, 3) == maxYCoord) == 2
            edgesInMaxY = cat(1, edgesInMaxY, transpose(edgesNodesCoord(:, 1)));
        end
   end
   
   edgesInMinX = largest_two_components(edgesInMinX);
   edgesInMaxX = largest_two_components(edgesInMaxX);
   edgesInMinY = largest_two_components(edgesInMinY);
   edgesInMaxY = largest_two_components(edgesInMaxY);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating lower and upper edges in MinX plane
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    graphMinX = graph(edgesInMinX(:, 1), edgesInMinX(:, 2), 'omitselfloops');
    plot(graphMinX)
    
    [minXBins, ~] = conncomp(graphMinX);
    edgesInMinX = cat(2, edgesInMinX, transpose(minXBins(edgesInMinX(:, 1))));
    
    lowerEdgesInMinX = [];
    upperEdgesInMinX = [];
    
    AdjacencyMinX = adjacency(graphMinX);
    degOneCoordMinX = nodesWithIdx(sum(AdjacencyMinX, 2) == 1, :);
    
    [~, lowerMinXidx] = min(degOneCoordMinX(:, 4));
    [~, upperMinXidx] = max(degOneCoordMinX(:, 4));
    
    lowerBegMinXNode = degOneCoordMinX(lowerMinXidx, 1);
    upperBegMinXNode = degOneCoordMinX(upperMinXidx, 1);
    
    lowerBool1 = edgesInMinX(edgesInMinX(:, 1) == lowerBegMinXNode, :);
    lowerBool1Comp = edgesInMinX(edgesInMinX(:, 1) == lowerBegMinXNode, 3);
    
    lowerBool2 = edgesInMinX(edgesInMinX(:, 2) == lowerBegMinXNode, :);
    lowerBool2Comp = edgesInMinX(edgesInMinX(:, 2) == lowerBegMinXNode, 3);
    
    upperBool1 = edgesInMinX(edgesInMinX(:, 1) == upperBegMinXNode, :);
    upperBool2 = edgesInMinX(edgesInMinX(:, 2) == upperBegMinXNode, :);

    if lowerBool1
        for ii = 1: size(edgesInMinX, 1)
            edge = edgesInMinX(ii, :);
            
            if edge(3) == lowerBool1Comp
                lowerEdgesInMinX = cat(1, lowerEdgesInMinX, edge);
            else
                upperEdgesInMinX = cat(1, upperEdgesInMinX, edge);
            end
        end

        oLowerMinX = lowerBool1(1: 2);%initializing ordered list
        lowerMinX = lowerEdgesInMinX(:, 1: 2);
        lowerMinX = largest_component(lowerMinX);
        [i, ~, ~] = find(lowerMinX(:, 1) == lowerBool1(1));%first edge idx
        lowerMinX(i, :) = [];%deleting first edge from edge list
    end

    if lowerBool2
        for ii = 1: size(edgesInMinX, 1)
            edge = edgesInMinX(ii, :);
            
            if edge(3) == lowerBool2Comp
                lowerEdgesInMinX = cat(1, lowerEdgesInMinX, edge);
            else
                upperEdgesInMinX = cat(1, upperEdgesInMinX, edge);
            end
        end

        oLowerMinX = fliplr(lowerBool2(1: 2));%initializing ordered list
        lowerMinX = lowerEdgesInMinX(:, 1: 2);
        lowerMinX = largest_component(lowerMinX);
        
        [i, ~, ~] = find(lowerMinX(:, 2) == lowerBool2(2));%first edge idx
        lowerMinX(i, :) = [];%deleting first edge from edge list
    end
    
    if upperBool1
        oUpperMinX = upperBool1(1: 2);%initializing ordered list
        upperMinX = upperEdgesInMinX(:, 1: 2);
        upperMinX = largest_component(upperMinX);
        
        [i, ~, ~] = find(upperMinX(:, 1) == upperBool1(1));%first edge idx
        upperMinX(i, :) = [];%deleting first edge from edge list
        
    elseif upperBool2
        oUpperMinX = fliplr(upperBool2(1: 2));%initializing ordered list
        upperMinX = upperEdgesInMinX(:, 1: 2);
        upperMinX = largest_component(upperMinX);
        
        [i, ~, ~] = find(upperMinX(:, 2) == upperBool2(2));%first edge idx
        upperMinX(i, :) = [];%deleting first edge from edge list
    end
    
    while size(lowerMinX, 1) > 0
        edge = oLowerMinX(end, :);

        if ismember(edge(2), lowerMinX(:, 1))
            [~, idx] = ismember(edge(2), lowerMinX(:, 1));
            newEdge = lowerMinX(idx, :);
        end
        if ismember(edge(2), lowerMinX(:, 2))
            [~, idx] = ismember(edge(2), lowerMinX(:, 2));
            newEdge = fliplr(lowerMinX(idx, :));
        end
        
        oLowerMinX = cat(1, oLowerMinX, newEdge);
        lowerMinX(idx, :) = [];
    end
        
    while size(upperMinX, 1) > 0
        edge = oUpperMinX(end, :);
        
        if ismember(edge(2), upperMinX(:, 1))
            [~, idx] = ismember(edge(2), upperMinX(:, 1));
            newEdge = upperMinX(idx, :);
        end
        if ismember(edge(2), upperMinX(:, 2))
            [~, idx] = ismember(edge(2), upperMinX(:, 2));
            newEdge = fliplr(upperMinX(idx, :));
        end
        
        oUpperMinX = cat(1, oUpperMinX, newEdge);
        upperMinX(idx, :) = [];
    end
    
    oLowerMinX = cat(1, oLowerMinX(:, 1), oLowerMinX(end, 2));
    oUpperMinX = cat(1, oUpperMinX(:, 1), oUpperMinX(end, 2));
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating lower and upper edges in max X plane
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    graphMaxX = graph(edgesInMaxX(:, 1), edgesInMaxX(:, 2), 'omitselfloops');
    [maxXBins, ~] = conncomp(graphMaxX);
    edgesInMaxX = cat(2, edgesInMaxX, transpose(maxXBins(edgesInMaxX(:, 1))));
    
    lowerEdgesInMaxX = [];
    upperEdgesInMaxX = [];
    
    AdjacencyMaxX = adjacency(graphMaxX);
    degOneCoordMaxX = nodesWithIdx(sum(AdjacencyMaxX, 2) == 1, :);
    [~, lowerMaxXidx] = min(degOneCoordMaxX(:, 4));
    [~, upperMaxXidx] = max(degOneCoordMaxX(:, 4));
    lowerBegMaxXNode = degOneCoordMaxX(lowerMaxXidx, 1);
    upperBegMaxXNode = degOneCoordMaxX(upperMaxXidx, 1);
    
    lowerBool1 = edgesInMaxX(edgesInMaxX(:, 1) == lowerBegMaxXNode, :);
    lowerBool1Comp = edgesInMaxX(edgesInMaxX(:, 1) == lowerBegMaxXNode, 3);
    lowerBool2 = edgesInMaxX(edgesInMaxX(:, 2) == lowerBegMaxXNode, :);
    lowerBool2Comp = edgesInMaxX(edgesInMaxX(:, 2) == lowerBegMaxXNode, 3);
    
    upperBool1 = edgesInMaxX(edgesInMaxX(:, 1) == upperBegMaxXNode, :);
    upperBool2 = edgesInMaxX(edgesInMaxX(:, 2) == upperBegMaxXNode, :);
    
    if lowerBool1
        for ii = 1: size(edgesInMaxX, 1)
            edge = edgesInMaxX(ii, :);
            
            if edge(3) == lowerBool1Comp
                lowerEdgesInMaxX = cat(1, lowerEdgesInMaxX, edge);
            else
                upperEdgesInMaxX = cat(1, upperEdgesInMaxX, edge);
            end
        end
        
        oLowerMaxX = lowerBool1(1: 2);%initializing ordered list
        lowerMaxX = lowerEdgesInMaxX(:, 1: 2);
        lowerMaxX = largest_component(lowerMaxX);
        
        [i, ~, ~] = find(lowerMaxX(:, 1) == lowerBool1(1));%first edge idx
        lowerMaxX(i, :) = [];%deleting first edge from edge list
        
        
    elseif lowerBool2
        for ii = 1: size(edgesInMaxX, 1)
            edge = edgesInMaxX(ii, :);
            
            if edge(3) == lowerBool2Comp
                lowerEdgesInMaxX = cat(1, lowerEdgesInMaxX, edge);
            else
                upperEdgesInMaxX = cat(1, upperEdgesInMaxX, edge);
            end
        end
        

        oLowerMaxX = fliplr(lowerBool2(1: 2));%initializing ordered list
        lowerMaxX = lowerEdgesInMaxX(:, 1: 2);
        lowerMaxX = largest_component(lowerMaxX);
        
        [i, ~, ~] = find(lowerMaxX(:, 2) == lowerBool2(2));%first edge idx
        lowerMaxX(i, :) = [];%deleting first edge from edge list
    end
    
    if upperBool1
        oUpperMaxX = upperBool1(1: 2);%initializing ordered list
        upperMaxX = upperEdgesInMaxX(:, 1: 2);
        upperMaxX = largest_component(upperMaxX);
 
        [i, ~, ~] = find(upperMaxX(:, 1) == upperBool1(1));%first edge idx
        upperMaxX(i, :) = [];%deleting first edge from edge list
        
    elseif upperBool2
        oUpperMaxX = fliplr(upperBool2(1: 2));%initializing ordered list
        upperMaxX = upperEdgesInMaxX(:, 1: 2);
        upperMaxX = largest_component(upperMaxX);
        
        [i, ~, ~] = find(upperMaxX(:, 2) == upperBool2(2));%first edge idx
        upperMaxX(i, :) = [];%deleting first edge from edge list
        
    end
    
    while size(lowerMaxX, 1) > 0
        edge = oLowerMaxX(end, :);
        
        if ismember(edge(2), lowerMaxX(:, 1))
            [~, idx] = ismember(edge(2), lowerMaxX(:, 1));
            newEdge = lowerMaxX(idx, :);
        end
        if ismember(edge(2), lowerMaxX(:, 2))
            [~, idx] = ismember(edge(2), lowerMaxX(:, 2));
            newEdge = fliplr(lowerMaxX(idx, :));
        end
        
        oLowerMaxX = cat(1, oLowerMaxX, newEdge);
        lowerMaxX(idx, :) = [];
    end
        
    while size(upperMaxX, 1) > 0
        edge = oUpperMaxX(end, :);
        
        if ismember(edge(2), upperMaxX(:, 1))
            [~, idx] = ismember(edge(2), upperMaxX(:, 1));
            newEdge = upperMaxX(idx, :);
        end
        if ismember(edge(2), upperMaxX(:, 2))
            [~, idx] = ismember(edge(2), upperMaxX(:, 2));
            newEdge = fliplr(upperMaxX(idx, :));
        end
        
        oUpperMaxX = cat(1, oUpperMaxX, newEdge);
        upperMaxX(idx, :) = [];
    end
    
    oLowerMaxX = cat(1, oLowerMaxX(:, 1), oLowerMaxX(end, 2));
    oUpperMaxX = cat(1, oUpperMaxX(:, 1), oUpperMaxX(end, 2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating lower and upper edges in min Y plane
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    graphMinY = graph(edgesInMinY(:, 1), edgesInMinY(:, 2), 'omitselfloops');
    [minYBins, ~] = conncomp(graphMinY);
    edgesInMinY = cat(2, edgesInMinY, transpose(minYBins(edgesInMinY(:, 1))));
    
    lowerEdgesInMinY = [];
    upperEdgesInMinY = [];
    
    AdjacencyMinY = adjacency(graphMinY);
    degOneCoordMinY = nodesWithIdx(sum(AdjacencyMinY, 2) == 1, :);
    [~, lowerMinYidx] = min(degOneCoordMinY(:, 4));
    [~, upperMinYidx] = max(degOneCoordMinY(:, 4));
    lowerBegMinYNode = degOneCoordMinY(lowerMinYidx, 1);
    upperBegMinYNode = degOneCoordMinY(upperMinYidx, 1);
    
    lowerBool1 = edgesInMinY(edgesInMinY(:, 1) == lowerBegMinYNode, :);
    lowerBool1Comp = edgesInMinY(edgesInMinY(:, 1) == lowerBegMinYNode, 3);
    lowerBool2 = edgesInMinY(edgesInMinY(:, 2) == lowerBegMinYNode, :);
    lowerBool2Comp = edgesInMinY(edgesInMinY(:, 2) == lowerBegMinYNode, 3);
    
    upperBool1 = edgesInMinY(edgesInMinY(:, 1) == upperBegMinYNode, :);
    upperBool2 = edgesInMinY(edgesInMinY(:, 2) == upperBegMinYNode, :);
    
    if lowerBool1
        for ii = 1: size(edgesInMinY, 1)
            edge = edgesInMinY(ii, :);
            
            if edge(3) == lowerBool1Comp
                lowerEdgesInMinY = cat(1, lowerEdgesInMinY, edge);
            else
                upperEdgesInMinY = cat(1, upperEdgesInMinY, edge);
            end
        end
        
        oLowerMinY = lowerBool1(1: 2);%initializing ordered list
        lowerMinY = lowerEdgesInMinY(:, 1: 2);
        lowerMinY = largest_component(lowerMinY);
        
        [i, ~, ~] = find(lowerMinY(:, 1) == lowerBool1(1));%first edge idx
        lowerMinY(i, :) = [];%deleting first edge from edge list
        
    elseif lowerBool2
        for ii = 1: size(edgesInMinY, 1)
            edge = edgesInMinY(ii, :);
            
            if edge(3) == lowerBool2Comp
                lowerEdgesInMinY = cat(1, lowerEdgesInMinY, edge);
            else
                upperEdgesInMinY = cat(1, upperEdgesInMinY, edge);
            end
        end
        

        oLowerMinY = fliplr(lowerBool2(1: 2));%initializing ordered list
        lowerMinY = lowerEdgesInMinY(:, 1: 2);
        lowerMinY = largest_component(lowerMinY);
        
        [i, ~, ~] = find(lowerMinY(:, 2) == lowerBool2(2));%first edge idx
        lowerMinY(i, :) = [];%deleting first edge from edge list
    end
    
    if upperBool1
        oUpperMinY = upperBool1(1: 2);%initializing ordered list
        upperMinY = upperEdgesInMinY(:, 1: 2);
        upperMinY = largest_component(upperMinY);
        
        [i, ~, ~] = find(upperMinY(:, 1) == upperBool1(1));%first edge idx
        upperMinY(i, :) = [];%deleting first edge from edge list
        
    elseif upperBool2
        oUpperMinY = fliplr(upperBool2(1: 2));%initializing ordered list
        upperMinY = upperEdgesInMinY(:, 1: 2);
        upperMinY = largest_component(upperMinY);
        
        [i, ~, ~] = find(upperMinY(:, 2) == upperBool2(2));%first edge idx
        upperMinY(i, :) = [];%deleting first edge from edge list
    end
    
    while size(lowerMinY, 1) > 0
        edge = oLowerMinY(end, :);
        
        if ismember(edge(2), lowerMinY(:, 1))
            [~, idx] = ismember(edge(2), lowerMinY(:, 1));
            newEdge = lowerMinY(idx, :);
        end
        if ismember(edge(2), lowerMinY(:, 2))
            [~, idx] = ismember(edge(2), lowerMinY(:, 2));
            newEdge = fliplr(lowerMinY(idx, :));
        end
        
        oLowerMinY = cat(1, oLowerMinY, newEdge);
        lowerMinY(idx, :) = [];
    end
        
    while size(upperMinY, 1) > 0
        edge = oUpperMinY(end, :);
        
        if ismember(edge(2), upperMinY(:, 1))
            [~, idx] = ismember(edge(2), upperMinY(:, 1));
            newEdge = upperMinY(idx, :);
        end
        if ismember(edge(2), upperMinY(:, 2))
            [~, idx] = ismember(edge(2), upperMinY(:, 2));
            newEdge = fliplr(upperMinY(idx, :));
        end
        
        oUpperMinY = cat(1, oUpperMinY, newEdge);
        upperMinY(idx, :) = [];
    end
    
    oLowerMinY = cat(1, oLowerMinY(:, 1), oLowerMinY(end, 2));
    oUpperMinY = cat(1, oUpperMinY(:, 1), oUpperMinY(end, 2));
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%creating lower and upper edges in max Y plane
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    graphMaxY = graph(edgesInMaxY(:, 1), edgesInMaxY(:, 2), 'omitselfloops');
    [maxYBins, ~] = conncomp(graphMaxY);
    edgesInMaxY = cat(2, edgesInMaxY, transpose(maxYBins(edgesInMaxY(:, 1))));
    
    lowerEdgesInMaxY = [];
    upperEdgesInMaxY = [];
    
    AdjacencyMaxY = adjacency(graphMaxY);
    degOneCoordMaxY = nodesWithIdx(sum(AdjacencyMaxY, 2) == 1, :);
    [~, lowerMaxYidx] = min(degOneCoordMaxY(:, 4));
    [~, upperMaxYidx] = max(degOneCoordMaxY(:, 4));
    lowerBegMaxYNode = degOneCoordMaxY(lowerMaxYidx, 1);
    upperBegMaxYNode = degOneCoordMaxY(upperMaxYidx, 1);
    
    lowerBool1 = edgesInMaxY(edgesInMaxY(:, 1) == lowerBegMaxYNode, :);
    lowerBool1Comp = edgesInMaxY(edgesInMaxY(:, 1) == lowerBegMaxYNode, 3);
    lowerBool2 = edgesInMaxY(edgesInMaxY(:, 2) == lowerBegMaxYNode, :);
    lowerBool2Comp = edgesInMaxY(edgesInMaxY(:, 2) == lowerBegMaxYNode, 3);
    
    upperBool1 = edgesInMaxY(edgesInMaxY(:, 1) == upperBegMaxYNode, :);
    upperBool2 = edgesInMaxY(edgesInMaxY(:, 2) == upperBegMaxYNode, :);
    
    if lowerBool1
        for ii = 1: size(edgesInMaxY, 1)
            edge = edgesInMaxY(ii, :);
            
            if edge(3) == lowerBool1Comp
                lowerEdgesInMaxY = cat(1, lowerEdgesInMaxY, edge);
            else
                upperEdgesInMaxY = cat(1, upperEdgesInMaxY, edge);
            end
        end
        
        oLowerMaxY = lowerBool1(1: 2);%initializing ordered list
        lowerMaxY = lowerEdgesInMaxY(:, 1: 2);
        lowerMaxY = largest_component(lowerMaxY);
        
        [i, ~, ~] = find(lowerMaxY(:, 1) == lowerBool1(1));%first edge idx
        lowerMaxY(i, :) = [];%deleting first edge from edge list
        
    elseif lowerBool2
        for ii = 1: size(edgesInMaxY, 1)
            edge = edgesInMaxY(ii, :);
            
            if edge(3) == lowerBool2Comp
                lowerEdgesInMaxY = cat(1, lowerEdgesInMaxY, edge);
            else
                upperEdgesInMaxY = cat(1, upperEdgesInMaxY, edge);
            end
        end

        oLowerMaxY = fliplr(lowerBool2(1: 2));%initializing ordered list
        lowerMaxY = lowerEdgesInMaxY(:, 1: 2);
        lowerMaxY = largest_component(lowerMaxY);
        
        [i, ~, ~] = find(lowerMaxY(:, 2) == lowerBool2(2));%first edge idx
        lowerMaxY(i, :) = [];%deleting first edge from edge list
    end
    
    if upperBool1
        oUpperMaxY = upperBool1(1: 2);%initializing ordered list
        upperMaxY = upperEdgesInMaxY(:, 1: 2);
        upperMaxY = largest_component(upperMaxY);
        
        [i, ~, ~] = find(upperMaxY(:, 1) == upperBool1(1));%first edge idx
        upperMaxY(i, :) = [];%deleting first edge from edge list
        
    elseif upperBool2
        oUpperMaxY = fliplr(upperBool2(1: 2));%initializing ordered list
        upperMaxY = upperEdgesInMaxY(:, 1: 2);
        upperMaxY = largest_component(upperMaxY);
        
        [i, ~, ~] = find(upperMaxY(:, 2) == upperBool2(2));%first edge idx
        upperMaxY(i, :) = [];%deleting first edge from edge list
    end
    
    while size(lowerMaxY, 1) > 0
        edge = oLowerMaxY(end, :);
        
        if ismember(edge(2), lowerMaxY(:, 1))
            [~, idx] = ismember(edge(2), lowerMaxY(:, 1));
            newEdge = lowerMaxY(idx, :);
        end
        if ismember(edge(2), lowerMaxY(:, 2))
            [~, idx] = ismember(edge(2), lowerMaxY(:, 2));
            newEdge = fliplr(lowerMaxY(idx, :));
        end
        
        oLowerMaxY = cat(1, oLowerMaxY, newEdge);
        lowerMaxY(idx, :) = [];
    end
        
    while size(upperMaxY, 1) > 0
        edge = oUpperMaxY(end, :);
        
        if ismember(edge(2), upperMaxY(:, 1))
            [~, idx] = ismember(edge(2), upperMaxY(:, 1));
            newEdge = upperMaxY(idx, :);
        end
        if ismember(edge(2), upperMaxY(:, 2))
            [~, idx] = ismember(edge(2), upperMaxY(:, 2));
            newEdge = fliplr(upperMaxY(idx, :));
        end
        
        oUpperMaxY = cat(1, oUpperMaxY, newEdge);
        upperMaxY(idx, :) = [];
    end
    
    oLowerMaxY = cat(1, oLowerMaxY(:, 1), oLowerMaxY(end, 2));
    oUpperMaxY = cat(1, oUpperMaxY(:, 1), oUpperMaxY(end, 2));
end

